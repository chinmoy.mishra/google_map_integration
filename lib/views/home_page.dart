import 'dart:async';

import 'package:flutter/material.dart';

import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:pretty_logger/pretty_logger.dart';
import 'package:share_plus/share_plus.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  // final Completer<GoogleMapController> _controller =
  //     Completer<GoogleMapController>();

  GoogleMapController? gMapController;

  double lat = 0.0;
  double len = 0.0;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(
      (timeStamp) async {
        await getLatLong();
        polyline();
      },
    );
    super.initState();

    // serviceStatusStream();
    // polyline();
    // markerIcon();
    // getCurrentLocation();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    gMapController!.dispose();
    super.dispose();
  }

  Future getLatLong() async {
    PLog.cyan("get latlen calling");
    bool service;
    LocationPermission permission;

    service = await Geolocator.isLocationServiceEnabled();

    if (!service) {
      PLog.red("Location service  are disabled");
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        PLog.red("Location permission are denied");
        PLog.red("permission denied");
      }
    }

    if (permission == LocationPermission.deniedForever) {
      PLog.red("Location are permanently denied");
      PLog.red("permission denied  forever");
    } else {
      Position currentPosition = await Geolocator.getCurrentPosition(
          // desiredAccuracy: LocationAccuracy.best,
          );
      PLog.red("permission submit");

      setState(() {
        lat = currentPosition.latitude;
        len = currentPosition.longitude;
      });
      addLocation("hhh", lat, len);
      gMapController != null
          ? gMapController!.animateCamera(
              CameraUpdate.newCameraPosition(
                CameraPosition(
                  target: LatLng(lat, len),
                ),
              ),
            )
          : const CircularProgressIndicator();
      PLog.green("Latitude :${lat}");
      PLog.green("Longitude :${len}");
    }
  }

  movingCamera(double latitude, double logtitude) async {
    gMapController != null
        ? await gMapController!.animateCamera(
            CameraUpdate.newCameraPosition(
              CameraPosition(
                  bearing: 192.8334901395799,
                  target: LatLng(latitude, logtitude),
                  tilt: 59.440717697143555,
                  zoom: 50.675),
            ),
          )
        : const CircularProgressIndicator();
  }

  // static const CameraPosition movingCameraTillDestination = CameraPosition(
  //     bearing: 192.8334901395799,
  //     target: LatLng(22.5788421, 88.4327307),
  //     tilt: 59.440717697143555,
  //     zoom: 19.151926040649414);

  Future<void> moving() async {
    gMapController != null
        ? await gMapController!.animateCamera(
            CameraUpdate.newCameraPosition(
              CameraPosition(
                  bearing: 100.8334901395799,
                  target: LatLng(lat, len),
                  tilt: 20.440717697143555,
                  zoom: 19.675),
            ),
          )
        : const CircularProgressIndicator();

    PLog.warning(lat.toString());
    PLog.red(len.toString());
  }

  Map<String, Marker> markers = {};

  addLocation(
    String id,
    double latitude,
    double longitude,
  ) async {
    PLog.green("add location $latitude $longitude");

    BitmapDescriptor markerImage = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(5, 5)), 'assets/images/3d-map.png');
    Marker locationMarker = Marker(
      markerId: MarkerId(id),
      position: LatLng(latitude, longitude),
      icon: markerImage,
    );
    PLog.green("add location $latitude $longitude");

    markers[id] = locationMarker;
    setState(() {});
  }

  desLocation(
    String id,
  ) async {
    BitmapDescriptor markerImage = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(size: Size(5, 5)), 'assets/images/marker.png');
    Marker locationMarker = Marker(
      markerId: MarkerId(id),
      position: LatLng(22.5788421, 88.4327307),
      icon: markerImage,
    );

    markers[id] = locationMarker;
    setState(() {});
  }

  LocationSettings locationSettings = const LocationSettings(
    accuracy: LocationAccuracy.high,
    distanceFilter: 0,
  );

  final Set<Polyline> _polyline = {};

  polyline() {
    PLog.cyan("poly---------$lat");
    PLog.cyan("poly---------$len");

    _polyline.add(
      Polyline(
        patterns: [
          PatternItem.dash(3),
        ],
        polylineId: const PolylineId('polyLine'),
        points: [
          LatLng(lat, len),
          const LatLng(22.5788421, 88.4327307),
        ],
        color: Colors.deepPurpleAccent,
        consumeTapEvents: true,
        endCap: Cap.roundCap,
        geodesic: true,
        jointType: JointType.round,
        startCap: Cap.roundCap,
        width: 4,
        zIndex: 1,
        visible: true,
      ),
    );
  }

  void _updatePosition(CameraPosition _position) {
    Position newMarkerPosition = Position(
        latitude: _position.target.latitude,
        longitude: _position.target.longitude,
        accuracy: 50.0,
        altitude: 12.1,
        heading: 12.0,
        speed: 1,
        speedAccuracy: 1,
        timestamp: DateTime.now());
    // Marker? marker = markers["1"];

    setState(() {
      lat = newMarkerPosition.latitude;
      len = newMarkerPosition.longitude;
    });
  }

// updateMerker(){
//  gMapController.
// }
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Map integration"),
          centerTitle: true,
          actions: [
            IconButton(
              icon: const Icon(Icons.share),
              onPressed: () {
                Share.share(
                    'https://www.google.com/maps/search/?api=1&query=$lat,$len');
              },
            ),
          ],
        ),
        body: Stack(children: [
          StreamBuilder(
              stream: Geolocator.getPositionStream(
                  locationSettings: AndroidSettings(
                accuracy: LocationAccuracy.best,
                distanceFilter: 0,
                forceLocationManager: true,
                // timeLimit: Duration(seconds: 10),
              )),
              builder: (
                context,
                AsyncSnapshot<Position> snapshot,
              ) {
                if (snapshot.connectionState == ConnectionState.done ||
                    snapshot.connectionState == ConnectionState.active) {
                  if (snapshot.hasError) {
                    PLog.red("error");
                    return const Text('Error');
                  } else if (snapshot.hasData) {
                    return GoogleMap(
                      onCameraMove: (position) {
                        CameraPosition(
                          target: LatLng(
                            snapshot.data!.latitude,
                            snapshot.data!.longitude,
                          ),
                        );

                        // setState(() {
                        //
                        // });

                        PLog.yellow(
                            "latitude on camera move  ${snapshot.data!.latitude.toString()}");
                        PLog.success(
                            "logitude on camera move  ${snapshot.data!.longitude.toString()}");
                      },
                      myLocationEnabled: true,
                      polylines: _polyline,
                      zoomControlsEnabled: true,
                      zoomGesturesEnabled: true,
                      compassEnabled: true,
                      markers: markers.values.toSet(),
                      mapType: MapType.normal,
                      initialCameraPosition: CameraPosition(
                        target: LatLng(
                            snapshot.data!.latitude, snapshot.data!.longitude),
                        zoom: 14.4746,
                      ),
                      onMapCreated: (GoogleMapController controller) {
                        gMapController = controller;

                        addLocation("movingCamera", snapshot.data!.latitude,
                            snapshot.data!.longitude);
                        desLocation("id");
                      },
                    );
                  } else {
                    return const Text('Empty data');
                  }
                } else if (snapshot.connectionState ==
                    ConnectionState.waiting) {
                  return const Center(
                      child: CircularProgressIndicator(
                    color: Colors.amber,
                  ));
                } else {
                  return Text("State:  ${snapshot.connectionState}");
                }
              })
        ]),
        floatingActionButton: FloatingActionButton(
          child: const Icon(Icons.map_outlined),
          onPressed: () {
            moving();

            // CameraPosition(target: LatLng(lat, len));

            // PLog.red("onpress latitude ${lat}");
            // PLog.red("onpress logitude ${len}");
          },
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      ),
    );
  }
}
